from analysis import Analysis
from enums import Person


if __name__ == '__main__':
    analysis = Analysis(Person.KEVIN)
    analysis.print_statistics()
    analysis.generate_graphs()
