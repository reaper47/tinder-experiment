import csv
import math
from collections import namedtuple, Counter
from enum import Enum
from functools import reduce
from typing import List
import numpy as np
from matplotlib.ticker import MaxNLocator
from prettytable import PrettyTable
import enums
import matplotlib.pyplot as plt

KEVIN_SHEET = 'data/kevin_sheet.csv'

StopWordStats = namedtuple('StopWords', ['nstops', 'sentence'])
Statistics = namedtuple('Statistics', ['mean', 'mode', 'max', 'min'])


class Girl:

    def __init__(self, name: str, age: int, description: str,
                 messaged_first: bool, super_liked: bool, looks: float,
                 hair_color: enums.HairColor, body_type: enums.BodyType,
                 ethnicity: enums.Ethnicity, first_message: str,
                 excites_you: str, other: str, continued_conversation: bool,
                 salut_reply: str):
        self.name = name
        self.age = age
        self.description = description
        self.messaged_first = messaged_first
        self.super_liked = super_liked
        self.looks = looks
        self.hair_color = hair_color
        self.body_type = body_type
        self.ethnicity = ethnicity
        self.first_message = first_message
        self.excites_you = excites_you
        self.other = other
        self.continued_conversation = continued_conversation
        self.salut_reply = salut_reply

    def __str__(self):
        return (f'<Girl {self.name} {self.age}:\n'
                f'\tDescription: {self.description}\n'
                f'\tMessaged First: {self.messaged_first}\n'
                f'\tFirst Message: {self.first_message}\n'
                f'\tSuper Liked: {self.super_liked}\n'
                f'\tLooks: {self.looks}\n'
                f'\tHair Color: {self.hair_color}\n'
                f'\tBody Type: {self.body_type}\n'
                f'\tEthnicity: {self.ethnicity}\n'
                f'\tWhat excites you: {self.excites_you}\n'
                f'\tOther: {self.other}\n'
                f'\tContinued Conversation: {self.continued_conversation}\n'
                f'\tSalut Reply: {self.salut_reply}>\n')


def load_kevin_data() -> List[Girl]:
    with open(KEVIN_SHEET) as f:
        reader = csv.reader(f, delimiter=',')
        next(reader)

        return [Girl(name=row[0],
                     age=int(row[1]),
                     description=row[2],
                     messaged_first=_is_yes(row[3]),
                     super_liked=_is_yes(row[4]),
                     looks=float(row[5].split('/')[0]),
                     hair_color=enums.HairColor.from_str(row[6]),
                     body_type=enums.BodyType.from_str(row[7]),
                     ethnicity=enums.Ethnicity.from_str(row[8]),
                     first_message=row[9],
                     excites_you=row[10],
                     other=row[11],
                     continued_conversation=_is_yes(row[12]),
                     salut_reply=row[13]) for row in reader]


def _is_yes(string: str) -> bool:
    return 'y' in string.lower()


def plot_frequencies(x: List, y: List, fname: str, xlabel=None,
                     is_horizontal=False, size=None, title=None) -> None:
    fig, ax = plt.subplots()

    if is_horizontal:
        ax.barh(y, x, align='center')
        ax.set_yticks(y)
        ax.set_xlabel('Frequency', fontsize=10)
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    else:
        plt.bar(x, y)
        ax.set_yticks(y)
        ax.set_ylabel('Frequency', fontsize=10)
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    if xlabel:
        ax.set_xlabel(xlabel)

    if size:
        fig.set_size_inches(*size)

    if title:
        ax.set_title(title, fontsize=18)

    ax.tick_params(axis='both', which='major', labelsize=10)
    fig.savefig(fname, bbox_inches='tight', dpi=300)


def plot_category(x: List, y: List, xlabel: str, ylabel: str,
                  fname: str, title=None) -> None:
    fig, ax = plt.subplots()
    ax.plot(x, y, label="dog")
    ax.set_title(title if title else '')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.grid(linestyle='--', linewidth=1)
    fig.savefig(fname, bbox_inches='tight', dpi=300)


def plot_pie_chart(sizes: List[float], labels: List[str],
                   fname: str, title=None, figsize=(15, 15)) -> None:
    fig, ax = plt.subplots(figsize=figsize)
    result = ax.pie(sizes, labels=['']*len(labels), autopct='%1.1f%%',
                    startangle=90, pctdistance=0.85, explode=[0.07]*len(sizes),
                    frame=True, labeldistance=1.05)

    for label, t in zip(labels, result[1]):
        x, y = t.get_position()
        angle = int(math.degrees(math.atan2(y, x)))
        ha = "left"
        va = "bottom"

        if angle > 90:
            angle -= 180

        if angle < 0:
            va = "top"

        if -45 <= angle <= 0:
            ha = "right"
            va = "bottom"

        plt.annotate(label, xy=(x, y), rotation=angle, ha=ha, va=va, size=8)

    if title:
        ax.set_title(title, fontsize=18)

    centre_circle = plt.Circle((0, 0), 0.70, fc='white')
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)
    ax.axis('equal')
    plt.tight_layout()

    fig.savefig(fname, bbox_inches='tight', dpi=300)


def plot_stacked_bar_chart_yes_no(counter, fname, title=None):
    N = len(counter.keys())

    yes, no = [], []
    for _, values in counter.items():
        filtered = list(filter(bool, values))
        ratio_yes = len(filtered) / len(values)
        ratio_no = 1 - ratio_yes
        yes.append(ratio_yes * 100)
        no.append(ratio_no * 100)

    ind = np.arange(N)  # the x locations for the groups
    width = 0.35  # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()
    p1 = ax.bar(ind, yes, width)
    p2 = ax.bar(ind, no, width, bottom=yes)

    if title:
        plt.title(title)

    plt.xticks(ind, (f'{x}/10' for x in counter.keys()))
    plt.yticks([])
    plt.legend((p1[0], p2[0]), ('Yes', 'No'))

    fig.savefig(fname, bbox_inches='tight', dpi=300)


def new_table(name: str, fnc):
    table = PrettyTable()
    table.field_names = [f'{name} Statistics', 'Value']
    add_rows_to_table(table, fnc())
    print(table)


def add_rows_to_table(table, rows: List) -> None:
    for row in rows:
        table.add_row(row)


def most_common_to_string(counter: Counter, num_elements: int):
    return ', '.join([x[0] for x in counter.most_common(num_elements)])


def get_most_common_keys(counter: Counter) -> str:
    most_common_value = counter.most_common(1)[0][1]
    most_common_keys = [str(key)
                        if not isinstance(key, Enum) else key.name.lower()
                        for key, value in counter.items()
                        if value == most_common_value]
    return ', '.join(most_common_keys)


def filter_with_bool_list(bool_list: List[int], other_list):
    return [item
            for bool_, item
            in zip(bool_list, other_list)
            if bool_]


def filter_string_from_list_(string, list_):
    return list(filter(lambda x: x.text != string, list_))


def create_counter_from_list(list1, list2):
    counter = {x: [] for x in set(list1)}
    for item1, item2 in zip(list1, list2):
        counter[item1].append(item2)

    for key, value in counter.items():
        counter[key] = filter_string_from_list_('-', value)

    return counter


"""
TEXT-RELATED
"""


def calculate_stops_per_text(sentences):
    stops_per_text = [0]*len(sentences)
    for idx, sentence in enumerate(sentences):
        for token in sentence:
            if token.is_stop:
                stops_per_text[idx] += 1
    return stops_per_text


def find_text_words_mode(text, num_els, to_str=False):
    words = clean_words(text)
    counter = Counter(words)
    if to_str:
        return most_common_to_string(counter, num_els)
    return counter.most_common(num_els)


def sentences_to_chunks(sentences: List[str]) -> str:
    return '\n'.join([f'({idx+1}) {string_to_chunks(sentence, 50)}'
                      for idx, sentence in enumerate(sentences)])


def string_to_chunks(string: str, size: int) -> str:
    return '\n'.join([string[i:i + size] for i in range(0, len(string), size)])


def get_sentences(sentences, stops: List[int], stop_value: int) -> List[str]:
    return [str(sentences[idx])
            for idx, num_stop in enumerate(stops)
            if num_stop == stop_value]


def delete_lists_from_indexes(lists, indexes: List[int]):
    lists = [l.copy() for l in lists]
    for idx in sorted(indexes, reverse=True):
        for l in lists:
            del l[idx]
    return lists


def clean_words(sentences) -> List[str]:
    words = get_clean_tokens(sentences)

    indexes = []
    for idx, word in enumerate(words):
        try:
            word.encode('ascii')
        except UnicodeEncodeError:
            indexes.append(idx)

    return delete_lists_from_indexes([words], indexes)[0]


def get_clean_tokens(sentences) -> List[str]:
    words = [token.text.lower()
             for description in sentences
             for token in description
             if not token.is_stop and not token.is_punct]
    return normalize_french_accents(words)


def normalize_french_accents(words):
    subs = (('é', 'e'), ('è', 'e'), ('à', 'a'), ('â', 'a'), ('ô', 'o'),
            ('’', '\''), ('ê', 'e'), ('û', 'u'), ('î', 'i'), ('ç', 'c'),
            ('ï', 'i'))
    return [reduce(lambda a, kv: a.replace(*kv), subs, word) for word in words]


def clean_excites_you_words(words: dict) -> None:
    keys_to_delete = ['aime', 'adore', ' ', 'dirais', 'faire',
                      'oui', "lorsqu'"]
    for key in keys_to_delete:
        if key in words:
            del words[key]
