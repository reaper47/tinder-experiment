from enum import Enum


class BodyType(Enum):
    THIN = 1
    SLIM = 2
    AVERAGE = 3
    FATTISH = 4
    FAT = 5
    NONE = 6

    @staticmethod
    def from_str(label):
        label = label.lower()
        if 'thin' in label:
            return BodyType.THIN
        elif 'slim' in label:
            return BodyType.SLIM
        elif 'average' in label:
            return BodyType.AVERAGE
        elif 'fattish' in label:
            return BodyType.FATTISH
        elif 'fat' in label:
            return BodyType.FAT
        else:
            return BodyType.NONE


class HairColor(Enum):
    BLONDE = 1
    LIGHT_BROWN = 2
    BROWN = 3
    DARK_BROWN = 4
    BLACK = 5
    TEINTED_GREY = 6
    TEINTED_PINK = 7
    TEINTED_RED = 8
    RED = 9
    NONE = 10

    @staticmethod
    def from_str(label):
        label = label.lower()
        if 'blond' in label:
            return HairColor.BLONDE
        elif 'light' in label and 'brown' in label:
            return HairColor.LIGHT_BROWN
        elif 'dark' in label and 'brown' in label:
            return HairColor.DARK_BROWN
        elif 'brown' in label:
            return HairColor.BROWN
        elif 'grey' in label:
            return HairColor.TEINTED_GREY
        elif 'pink' in label:
            return HairColor.TEINTED_PINK
        elif 'teint' in label and 'red' in label:
            return HairColor.TEINTED_RED
        elif 'red' in label:
            return HairColor.RED
        else:
            return HairColor.NONE


class Ethnicity(Enum):
    WHITE = 1
    BLACK = 2
    ASIAN = 3
    INUIT = 4
    NONE = 5

    @staticmethod
    def from_str(label):
        label = label.lower()
        if 'white' in label:
            return Ethnicity.WHITE
        elif 'black' in label:
            return Ethnicity.BLACK
        elif 'asian' in label:
            return Ethnicity.ASIAN
        elif 'inuit' in label:
            return Ethnicity.INUIT
        else:
            return Ethnicity.NONE


class Person(Enum):
    KEVIN = 1
    ALEXANDER = 2
    ALEXANDRA = 3
