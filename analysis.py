import os
import statistics
from collections import Counter
from typing import Tuple, List
import operator
from emoji import UNICODE_EMOJI
import utils
import enums
import spacy


class Analysis:

    def __init__(self, person: enums.Person):
        if person == enums.Person.KEVIN:
            self.data = utils.load_kevin_data()
        else:
            self.data = utils.load_kevin_data()

        self.person = person
        self.nlp = spacy.load('fr_core_news_sm')
        self.ages = [x.age for x in self.data]
        self.names = [x.name.title().replace('É', 'E') for x in self.data]
        self.messaged_first = [x.messaged_first for x in self.data]
        self.super_likes = [x.super_liked for x in self.data]
        self.first_messages = [self.nlp(x.first_message) for x in self.data
                               if x.first_message != '-']
        self.first_messages[0] = self.nlp("Mais d'où sors-tu beau garçon.")
        self.ethnicities = [x.ethnicity for x in self.data]
        self.looks = [x.looks for x in self.data]
        self.hair_colors = [x.hair_color for x in self.data]

        self.descriptions = [self.nlp(x.description) for x in self.data]
        self.descr_stops = utils.calculate_stops_per_text(self.descriptions)

        self.excites = [self.nlp(x.excites_you) for x in self.data]
        self.excites_stops = utils.calculate_stops_per_text(self.excites)
        self.continued_conversations = [x.continued_conversation
                                        for x in self.data]
        self.saluts = [self.nlp(x.salut_reply) for x in self.data]

        self.graphs_folder = f'graphs/{person.name.lower()}'
        if not os.path.exists(self.graphs_folder):
            os.mkdir(self.graphs_folder)

    """
    AGE
    """

    def calculate_age_average(self) -> int:
        return int(round(statistics.mean(self.ages), 0))

    def calculate_age_median(self) -> int:
        return int(round(statistics.median(self.ages), 0))

    def calculate_age_mode(self) -> int:
        return int(round(statistics.mode(self.ages), 0))

    def calculate_age_stdev(self) -> int:
        return int(round(statistics.stdev(self.ages), 0))

    def find_age_max(self) -> int:
        return max(self.ages)

    def find_age_min(self) -> int:
        return min(self.ages)

    """
    NAME
    """

    def find_most_common_name(self) -> str:
        counter = Counter(self.names)
        most_common = counter.most_common()
        most_common_frequency = most_common[0][1]
        return ', '.join([name for name, frequency in most_common
                          if frequency == most_common_frequency])

    def find_longest_name(self) -> str:
        return max(self.names, key=len)

    def find_shortest_name(self) -> str:
        return min(self.names, key=len)

    """
    DESCRIPTION
    """

    def find_description_word_mode(self):
        return utils.find_text_words_mode(self.descriptions, 5, True)

    def find_description_stop_words_max(self) -> utils.StopWordStats:
        idx, value = max(enumerate(self.descr_stops),
                         key=operator.itemgetter(1))

        sentences = utils.get_sentences(self.descriptions, self.descr_stops,
                                        value)
        sentences = utils.sentences_to_chunks(sentences)
        return utils.StopWordStats(nstops=value, sentence=sentences)

    def find_description_stop_words_min(self) -> utils.StopWordStats:
        indexes = [idx for idx, x in enumerate(self.descr_stops) if x == 0]
        lists_delete_from = [self.descriptions, self.descr_stops]
        lists = utils.delete_lists_from_indexes(lists_delete_from, indexes)

        descriptions, stops = lists[0], lists[1]
        _, value = min(enumerate(stops), key=operator.itemgetter(1))

        sentences = utils.get_sentences(descriptions, stops, stop_value=value)
        sentences = utils.sentences_to_chunks(sentences)
        return utils.StopWordStats(nstops=value, sentence=sentences)

    def find_description_emoticons_most_common(self, num_els, to_str=False):
        words = utils.get_clean_tokens(self.descriptions)

        emoticons = []
        for word in words:
            try:
                word.encode('ascii')
            except UnicodeEncodeError:
                if word in UNICODE_EMOJI:
                    emoticons.append(word)

        counter = Counter(emoticons)
        if to_str:
            return utils.most_common_to_string(counter, num_els)
        return counter.most_common(num_els)

    def find_description_emoticons_stats(self) -> utils.Statistics:
        num_emoticons_per_text = [0]*len(self.descriptions)
        for idx, description in enumerate(self.descriptions):
            for c in str(description):
                if c in UNICODE_EMOJI:
                    num_emoticons_per_text[idx] += 1

        mean = int(round(statistics.mean(num_emoticons_per_text), 0))
        mode = int(round(statistics.mode(num_emoticons_per_text), 0))
        max_ = max(num_emoticons_per_text)
        return utils.Statistics(mean=mean, mode=mode, min=None, max=max_)

    def find_description_sentence_length(self) -> utils.Statistics:
        sentence_lengths = []
        for description in enumerate(self.descriptions):
            sentence_lengths.append(len(str(description)))

        mean = int(round(statistics.mean(sentence_lengths), 0))
        mode = int(round(statistics.mode(sentence_lengths), 0))
        max_ = max(sentence_lengths)
        return utils.Statistics(mean=mean, mode=mode, min=None, max=max_)

    """
    MESSAGED FIRST
    """

    def calculate_percentage_messaged_first(self) -> float:
        counter = Counter(self.messaged_first)
        return counter[True]/counter[False]*100

    def calculate_percentage_messaged_first_super_liked(self) -> float:
        counter = 0
        for first, super_like in zip(self.messaged_first, self.super_likes):
            if first and super_like:
                counter += 1

        counter_supers = Counter(self.super_likes)
        return counter/counter_supers[True]*100

    def calculate_percentage_super_liked(self) -> float:
        counter = Counter(self.super_likes)
        return counter[True] / counter[False] * 100

    def find_messaged_first_words_mean(self) -> int:
        messages = [list(map(str, self.first_messages))]
        num_words = list(map(lambda x: len(str(x).split(' ')), messages))
        return int(round(statistics.mean(num_words), 0))

    def find_messaged_first_word_most_common(self, num_els=5) -> str:
        words = utils.get_clean_tokens(self.first_messages)
        counter = Counter(words)
        return utils.most_common_to_string(counter, num_els)

    def calculate_first_messages_length_mean(self) -> int:
        lengths = list(map(lambda x: len(str(x).split(' ')),
                           self.first_messages))
        return int(round(statistics.mean(lengths), 0))

    def calculate_first_messages_length_mode(self) -> str:
        lengths = list(map(lambda x: len(str(x).split(' ')),
                           self.first_messages))
        counter = Counter(lengths)
        return utils.get_most_common_keys(counter)

    def find_first_message_hair_color(self) -> str:
        hair_colors = [color.name.lower()
                       for like, color
                       in zip(self.messaged_first, self.hair_colors)
                       if like]
        counter = Counter(hair_colors)
        return utils.get_most_common_keys(counter)

    def find_first_message_ethnicity(self) -> str:
        ethnicities = [ethnicity.name.lower()
                       for like, ethnicity
                       in zip(self.messaged_first, self.ethnicities)
                       if like]
        counter = Counter(ethnicities)
        return utils.get_most_common_keys(counter)

    def find_first_message_looks(self) -> str:
        looks = self.__filter_looks_with_first_messages()
        counter = Counter(looks)
        return utils.get_most_common_keys(counter)

    """
    LOOKS
    """

    def max_looks_hot_messaged_first(self) -> float:
        looks = self.__filter_looks_with_first_messages()
        return max(looks)

    def min_looks_hot_messaged_first(self):
        looks = self.__filter_looks_with_first_messages()
        return min(looks)

    def percentage_looks_hot_messaged_first(self) -> float:
        looks = self.__filter_looks_with_first_messages()
        counter = Counter(looks)
        return counter[max(looks)]/len(looks)*100

    def percentage_looks_ugly_messaged_first(self) -> float:
        looks = self.__filter_looks_with_first_messages()
        counter = Counter(looks)
        min_ = min(list(filter(lambda x: x > 0, looks)))
        return counter[min_]/len(looks)*100

    def __filter_looks_with_first_messages(self):
        return [look
                for like, look
                in zip(self.messaged_first, self.looks)
                if like]

    def calculate_looks_mode(self) -> float:
        return statistics.mode(self.looks)

    def calculate_looks_mean(self) -> float:
        return round(statistics.mean(self.looks)*2) / 2

    def percentage_looks_no_pics(self) -> float:
        counter = Counter(self.looks)
        return counter[0.0]/len(self.looks)*100

    def percentage_looks_perfect(self):
        counter = Counter(self.looks)
        return counter[10.0]/len(self.looks)*100

    """
    HAIR COLOR
    """

    def find_hair_color_mode(self) -> str:
        counter = Counter(self.hair_colors)
        return utils.get_most_common_keys(counter)

    def find_hair_color_messaged_first_mode(self) -> str:
        colors = utils.filter_with_bool_list(self.messaged_first,
                                             self.hair_colors)
        counter = Counter(colors)
        return utils.get_most_common_keys(counter)

    def find_hair_color_mode_per_look_category(self):
        looks_counter = Counter(self.looks)

        counter_hair = {x: [] for x in looks_counter.keys()}
        for look, hair_color in zip(self.looks, self.hair_colors):
            counter_hair[look].append(hair_color)
        counter_hair = dict(sorted(counter_hair.items()))

        for key, colors in counter_hair.items():
            counter = Counter(colors)
            counter_hair[key] = utils.get_most_common_keys(counter)

        del counter_hair[0.0]
        return counter_hair

    """
    ETHNICITY
    """

    def find_ethnicity_mode(self):
        counter = Counter(self.ethnicities)
        return utils.get_most_common_keys(counter)

    def find_ethnicity_mode_messaged_first(self):
        ethnicities = utils.filter_with_bool_list(self.messaged_first,
                                                  self.ethnicities)
        counter = Counter(ethnicities)
        return utils.get_most_common_keys(counter)

    """
    EXCITES YOU
    """

    def find_excites_mode(self) -> str:
        words = dict(utils.find_text_words_mode(self.excites, 20))
        utils.clean_excites_you_words(words)
        return ', '.join([x for x in list(words.keys())[:5]])

    def find_excites_first_message_mode(self) -> str:
        excites = utils.filter_with_bool_list(self.messaged_first,
                                              self.excites)
        excites = utils.filter_string_from_list_('-', excites)

        words = dict(utils.find_text_words_mode(excites, 20))
        utils.clean_excites_you_words(words)
        return ', '.join([x for x in list(words.keys())[:5]])

    def find_excites_mode_per_ethnicity(self) -> str:
        counter = utils.create_counter_from_list(self.ethnicities,
                                                 self.excites)

        del_keys = [key for key, val in counter.items() if not val]
        for key in del_keys:
            del counter[key]
        del counter[enums.Ethnicity.NONE]

        tuples = []
        for ethnicity, excites in counter.items():
            words = dict(utils.find_text_words_mode(excites, 20))
            utils.clean_excites_you_words(words)
            mode = ', '.join([x for x in list(words.keys())[:5]])
            tuples.append((ethnicity.name.lower(), mode))
        return tuples

    def find_excites_answer_length_stats(self) -> utils.Statistics:
        excites = utils.filter_string_from_list_('-', self.excites)
        excites_length = list(map(len, excites))
        mean = int(round(statistics.mean(excites_length), 0))
        mode = statistics.mode(excites_length)
        return utils.Statistics(mean=mean, mode=mode, max=None, min=None)

    def find_excites_mode_per_look(self):
        counter = utils.create_counter_from_list(self.looks, self.excites)
        del counter[0.0]

        tuples = []
        for look, excites in counter.items():
            words = dict(utils.find_text_words_mode(excites, 20))
            utils.clean_excites_you_words(words)
            mode = ', '.join([x for x in list(words.keys())[:5]])
            tuples.append((look, mode))
        return tuples

    def percentage_excites_replied(self) -> float:
        no_replies = list(filter(lambda x: x.text == '-', self.excites))
        return len(no_replies)/len(self.excites)*100

    def percentage_excites_avoided_for_who_replied(self) -> float:
        excites = utils.filter_string_from_list_('-', self.excites)
        if self.person == enums.Person.KEVIN:
            return 5/len(excites)*100
        return 0

    """
    CONTINUED CONVERSATION
    """

    def percentage_continued_conversation(self):
        continued = list(filter(bool, self.continued_conversations))
        return len(continued)/len(self.continued_conversations)*100

    def percentage_continued_conversation_replied(self):
        filtered = utils.filter_with_bool_list(self.messaged_first,
                                               self.continued_conversations)
        continued = list(filter(bool, filtered))
        return len(continued)/len(filtered) * 100

    def percentage_continued_conversation_superliked(self):
        filtered = utils.filter_with_bool_list(self.super_likes,
                                               self.continued_conversations)
        continued = list(filter(bool, filtered))
        return len(continued) / len(filtered) * 100

    def percentage_continued_conversation_just_questions(self):
        filtered = list(filter(bool, self.continued_conversations))
        if self.person == enums.Person.KEVIN:
            return 21/len(filtered)*100
        return -1

    def find_continued_conversation_mode_per_look(self):
        counter = {n: [] for n in set(self.looks)}
        for looks, continued in zip(self.looks, self.continued_conversations):
            counter[looks].append(continued)
        del counter[0.0]

        tuples = []
        for look, continued in counter.items():
            tuples.append((look, Counter(continued).most_common(1)[0][0]))
        return tuples

    """
    SALUT REPLY
    """

    def percentage_salut_replied(self):
        filtered = list(filter(lambda x: x.text not in ['-', 'N/A'],
                               self.saluts))

        if self.person == enums.Person.KEVIN:
            num_missing_samples = 60
        else:
            num_missing_samples = 0

        return len(filtered)/(len(self.saluts)-num_missing_samples)*100

    def find_salut_mode_words(self):
        filtered = self.__normalize_saluts()
        return ', '.join([x[0] for x in Counter(filtered).most_common(5)])

    def percentage_reply_continued(self):
        filtered = self.__normalize_saluts()
        count = 0
        for x in filtered:
            if '?' in x:
                count += 1
        return count/len(filtered)*100

    def find_reply_mode_per_look(self):
        pass

    def __normalize_saluts(self):
        filtered = filter(lambda x: x.text not in ['-', 'N/A'],
                          self.saluts)
        filtered = list(map(lambda x: x.text.lower(), filtered))
        return utils.normalize_french_accents(filtered)

    """
    GRAPHS
    """

    def generate_graphs(self) -> None:
        print('\nGenerating graphs')
        print('Name distribution...')
        self.graph_name_distribution()
        print('Age distribution...')
        self.graph_age_distribution()
        print('Words distribution...')
        self.graph_words_distribution()
        print('Emoticons distribution...')
        self.graph_emoticons_distribution()
        print('Hair color pie chart...')
        self.graph_hair_color_pie_chart()
        print('Hair color per look category pie chart...')
        self.graph_hair_color_per_look_category()
        print('Ethnicity pie chart...')
        self.graph_ethnicity_pie_chart()
        print('What excites you words distribution...')
        self.graph_excites_distribution()
        print('Salut category pie chart...')
        self.graph_salut_replied_pie_chart()
        print('Salut continued conversation pie chart...')
        self.graph_salut_continued_conversation_pie_chart()
        print("Done")

    def graph_name_distribution(self) -> None:
        counter = Counter(self.names)
        names, freqs = zip(*sorted(zip(counter.keys(), counter.values())))
        utils.plot_frequencies(freqs, names,
                               f'{self.graphs_folder}/name_distribution.png',
                               title='Name Distribution', size=(30, 30),
                               is_horizontal=True)

    def graph_age_distribution(self) -> None:
        counter = Counter(self.ages)
        ages, frequencies = list(counter.keys()), list(counter.values())
        utils.plot_frequencies(ages, frequencies,
                               f'{self.graphs_folder}/age_distribution.png',
                               xlabel='Age', title='Age Distribution')

    def graph_emoticons_distribution(self) -> None:
        emoticons = self.find_description_emoticons_most_common(30, False)
        emoticons, frequencies = zip(*emoticons)

        utils.plot_frequencies(frequencies, emoticons,
                               (f'{self.graphs_folder}/'
                                'emoticons_distribution.png'),
                               title='Emoticon Distribution', size=(30, 30),
                               is_horizontal=True)

    def graph_words_distribution(self) -> None:
        words = utils.find_text_words_mode(self.descriptions, 30, False)
        words, frequencies = zip(*words)
        utils.plot_frequencies(frequencies, words,
                               f'{self.graphs_folder}/words_distribution.png',
                               title='Word Distribution', size=(30, 30),
                               is_horizontal=True)

    def graph_hair_color_per_look_category(self) -> None:
        hair_colors_category = self.find_hair_color_mode_per_look_category()
        sizes = list(hair_colors_category.keys())
        labels = list(hair_colors_category.values())
        fname = f'{self.graphs_folder}/hair_looks_categories.png'
        utils.plot_category(sizes, labels, 'Looks Rating', '',
                            fname=fname, title='Hair Color per Look Category')

    def graph_hair_color_pie_chart(self) -> None:
        counter = Counter(self.hair_colors)
        del counter[enums.HairColor.NONE]
        labels = [x.name.lower().replace('_', ' ') for x in counter.keys()]
        sizes = [x/len(self.hair_colors)*100 for x in counter.values()]
        utils.plot_pie_chart(sizes, labels,
                             f'{self.graphs_folder}/hair_categories.png',
                             'Hair Colors')

    def graph_ethnicity_pie_chart(self) -> None:
        counter = Counter(self.ethnicities)
        del counter[enums.Ethnicity.NONE]
        labels = [x.name.lower() for x in counter.keys()]
        sizes = [x/len(self.ethnicities)*100 for x in counter.values()]
        utils.plot_pie_chart(sizes, labels,
                             f'{self.graphs_folder}/ethnicity_categories.png',
                             'Ethnicity Distribution')

    def graph_excites_distribution(self) -> None:
        words = dict(utils.find_text_words_mode(self.excites, 30, False))
        utils.clean_excites_you_words(words)
        words = [(key, value) for key, value in words.items()]

        words, frequencies = zip(*words)
        fname = f'{self.graphs_folder}/excites_you_word_distribution.png'
        utils.plot_frequencies(frequencies, words, fname,
                               title='Excites You Word Distribution',
                               size=(30, 30), is_horizontal=True)

    def graph_excites_per_look_category(self) -> None:
        categories = dict(self.find_excites_mode_per_look())
        sizes = list(categories.keys())
        labels = list(categories.values())
        utils.plot_category(sizes, labels, 'Looks Rating', '',
                            fname=f'{self.graphs_folder}/excites_categs.png',
                            title='Excites per Look Category')

    def graph_continued_conversation(self) -> None:
        counter = Counter(self.continued_conversations)

        labels = [str(x) for x in counter.keys()]
        sizes = [x/len(self.continued_conversations)*100
                 for x in counter.values()]

        fname = f'{self.graphs_folder}/continued_conversation.png',
        utils.plot_pie_chart(sizes, labels, fname=fname,
                             title='Continued Conversation')

    def graph_continued_conversation_by_look(self) -> None:
        counter = {n: [] for n in set(self.looks)}
        for looks, continued in zip(self.looks, self.continued_conversations):
            counter[looks].append(continued)
        del counter[0.0]

        title = 'Continued Convesation by Looks'
        fname = f'{self.graphs_folder}/continued_by_looks.png'
        utils.plot_stacked_bar_chart_yes_no(counter, title=title, fname=fname)

    def graph_salut_replied_pie_chart(self) -> None:
        yes = self.percentage_salut_replied()
        no = 100 - yes

        labels = ['Yes', 'No']
        sizes = [yes, no]

        utils.plot_pie_chart(sizes, labels,
                             f'{self.graphs_folder}/salut_replied.png',
                             'Salut Replied Back', figsize=(7, 7))

    def graph_salut_continued_conversation_pie_chart(self) -> None:
        yes = self.percentage_reply_continued()
        no = 100 - yes

        labels = ['Yes', 'No']
        sizes = [yes, no]

        title = 'Salut Continued Conversation Without Me Initiating'
        utils.plot_pie_chart(sizes, labels,
                             f'{self.graphs_folder}/salut_continued.png',
                             title=title, figsize=(7, 7))

    """
    STATISTICS
    """

    def print_statistics(self) -> None:
        utils.new_table('Age', self.get_age_statistics)
        utils.new_table('Name', self.get_name_statistics)
        utils.new_table('Description', self.get_description_statistics)
        utils.new_table('Messaged First', self.get_messaged_first_statistics)
        utils.new_table('Looks', self.get_looks_statistics)
        utils.new_table('Hair Color', self.get_hair_color_statistics)
        utils.new_table('Ethnicity', self.get_ethnicity_statistics)
        utils.new_table('What Excites You', self.get_excites_you_statistics)
        utils.new_table('Continued Conversation',
                        self.get_continued_conversation_statistics)
        utils.new_table('Salut Reply', self.get_salut_reply_statistics)

    def get_age_statistics(self) -> List[Tuple[str, int]]:
        return [('Median', self.calculate_age_median()),
                ('Average', self.calculate_age_average()),
                ('Mode', self.calculate_age_mode()),
                ('Stdev', self.calculate_age_stdev()),
                ('Max', self.find_age_max()),
                ('Min', self.find_age_min())]

    def get_name_statistics(self) -> List[Tuple[str, str]]:
        return [('Most Common', self.find_most_common_name()),
                ('Longest', self.find_longest_name()),
                ('Shortest', self.find_shortest_name())]

    def get_description_statistics(self):
        mode_words = self.find_description_word_mode()
        common_emojis = self.find_description_emoticons_most_common(5, True)
        most_stop_words = self.find_description_stop_words_max()
        least_stop_words = self.find_description_stop_words_min()
        emoticons_stats = self.find_description_emoticons_stats()
        sentence_stats = self.find_description_sentence_length()

        return [('Most common words', mode_words),
                ('Mean sentence length', sentence_stats.mean),
                ('Mode sentence length', sentence_stats.mode),
                ('Max sentence length', sentence_stats.max),
                ('', ''),
                ('Most common emoticons', common_emojis),
                ('Mean emoticons', emoticons_stats.mean),
                ('Mode emoticons', emoticons_stats.mode),
                ('Max emoticons', emoticons_stats.max),
                ('', ''),
                ('Max number of stop words', most_stop_words.nstops),
                ('Corresponding text', most_stop_words.sentence),
                ('', ''),
                ('Min number of stop words', least_stop_words.nstops),
                ('Corresponding text', least_stop_words.sentence)]

    def get_messaged_first_statistics(self):
        percentage_messaged_first = self.calculate_percentage_messaged_first()
        percentage_super_liked = self.calculate_percentage_super_liked()
        super_liked = self.calculate_percentage_messaged_first_super_liked()
        most_common_words = self.find_messaged_first_word_most_common(5)
        first_message_length_mode = self.calculate_first_messages_length_mode()
        first_message_length_mean = self.calculate_first_messages_length_mean()
        first_message_hair_color = self.find_first_message_hair_color()

        return [('% who messaged first', f'{percentage_messaged_first:.2f}%'),
                ('% who super liked', f'{percentage_super_liked:.2f}%'),
                ('% who messaged first + super liked', f'{super_liked:.2f}%'),
                ('', ''),
                ('Words mean', self.find_messaged_first_words_mean()),
                ('Most common words', most_common_words),
                ('Number of words mode', first_message_length_mode),
                ('Number of words mean', first_message_length_mean),
                ('', ''),
                ('Most common ethnicity', self.find_first_message_ethnicity()),
                ('Most common looks', self.find_first_message_looks()),
                ('Most common hair color', first_message_hair_color)]

    def get_looks_statistics(self):
        hot_messaged = f'{self.percentage_looks_hot_messaged_first():.2f}%'
        ugly_messaged = f'{self.percentage_looks_ugly_messaged_first():.2f}%'
        max_hot_message_first = self.max_looks_hot_messaged_first()

        return [('Mode', self.calculate_looks_mode()),
                ('Mean', self.calculate_looks_mean()),
                ('', ''),
                ('Hottest messaged first', max_hot_message_first),
                ('Ugly messaged first', self.min_looks_hot_messaged_first()),
                ('', ''),
                ('% no pics', f'{self.percentage_looks_no_pics():.2f}%'),
                ('% 10/10 looks', f'{self.percentage_looks_perfect():.2f}%'),
                ('% hot who messaged first', hot_messaged),
                ('% unattractive who messaged first', ugly_messaged)]

    def get_hair_color_statistics(self):
        color_messaged_first = self.find_hair_color_messaged_first_mode()
        color_look_category = self.find_hair_color_mode_per_look_category()
        category_formatted = [(f'{key}/10', value)
                              for key, value in color_look_category.items()]

        return [('Mode', self.find_hair_color_mode()),
                ('Mode of who messaged first', color_messaged_first),
                ('', ''),
                ('Mode per looks rating:', ''),
                *category_formatted]

    def get_ethnicity_statistics(self):
        ethnicity_messaged_first = self.find_ethnicity_mode_messaged_first()

        return [('Mode', self.find_ethnicity_mode()),
                ('Mode of who messaged first', ethnicity_messaged_first)]

    def get_excites_you_statistics(self):
        first_message_mode = self.find_excites_first_message_mode()
        answer_length_stats = self.find_excites_answer_length_stats()
        excites_avoided = self.percentage_excites_avoided_for_who_replied()

        ethnicity_category = self.find_excites_mode_per_ethnicity()
        looks_category = self.find_excites_mode_per_look()
        looks_formatted = [(f'{key}/10', value)
                           for key, value in looks_category]

        return [('% who replied', f'{self.percentage_excites_replied():.2f}%'),
                ('% avoided the question + replied)',
                 f'{excites_avoided:.2f}%'),
                ('Mode', self.find_excites_mode()),
                ('Mode from messaged first', first_message_mode),
                ('Answer length mode', answer_length_stats.mode),
                ('Answer length mode', answer_length_stats.mean),
                ('', ''),
                ('Mode per ethnicity:', ''),
                *ethnicity_category,
                ('', ''),
                ('Mode words per looks rating:', ''),
                *looks_formatted]

    def get_continued_conversation_statistics(self):
        continued = self.percentage_continued_conversation()
        continued_replied = self.percentage_continued_conversation_replied()
        continued_super = self.percentage_continued_conversation_superliked()
        answered_questions = self.\
            percentage_continued_conversation_just_questions()

        looks_category = self.find_continued_conversation_mode_per_look()
        looks_formatted = [(f'{key}/10', 'Yes' if value else 'No')
                           for key, value in looks_category]

        return [('% continued', f'{continued:.2f}%'),
                ('% continued + messaged first', f'{continued_replied:.2f}%'),
                ('% continued + super liked', f'{continued_super:.2f}%'),
                ('% just answered questions', f'{answered_questions:.2f}%'),
                ('', ''),
                ('Continued by looks rating:', ''),
                *looks_formatted]

    def get_salut_reply_statistics(self):
        continued = self.percentage_reply_continued()

        return [('% replied back', f'{self.percentage_salut_replied():.2f}%'),
                ('Mode reply', self.find_salut_mode_words()),
                ('% continued without me asking', f'{continued:.2f}%')]
